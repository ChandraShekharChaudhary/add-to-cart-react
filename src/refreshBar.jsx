
export default function RefreshBar({counters,setCounters}) {
  function resetList(){
    if(counters.length===0){
      setCounters([
        { value: 0, key: 0 },
        { value: 0, key: 1 },
        { value: 0, key: 2 },
        { value: 0, key: 3 },
      ]);
    }
  }

  function resetCounts(){
    let resetCounters=[...counters];
    resetCounters.forEach((data,index)=> resetCounters[index].value=0);
    setCounters(resetCounters);
  }

  return (
    <div className="refreshBar">
      <button className="refreshList" onClick={()=> resetCounts()}>
        <img src="/refresh.png" alt="" className="refreshImg" />
      </button>
      <button className="refreshPage" onClick={()=> resetList()}>
        <img src="/recycle.png" alt="" className="recycleImg" />
      </button>
    </div>
  );
}

