import Counter from "./counter";

export default function CounterList({ counters, setCounters }) {
  return counters.map((counter,index) => (
      <Counter
      index={index}
      countid={counter.key}
      counters={counters}
      setCounters={setCounters}
    />
  ));
}
