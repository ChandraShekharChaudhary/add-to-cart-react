import { useState } from "react";
export default function Navebar({counters}){

    return <div className="navebar">
        <img src="/cart-black.png" alt="err" className="cartImg" />
        <div className="numItems">{counters.reduce((acc,counter)=>{
            if(counter.value>0){
                acc++;
            }
            return acc;
        },0)}</div>
        <div className="items">Items</div>
    </div>
}