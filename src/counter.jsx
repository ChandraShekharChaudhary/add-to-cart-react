import React, { useState } from "react";

export default function Counter({index,countid, counters, setCounters }) {
  function increment(countid,index) {
    let incrementedCounters = [...counters];
    incrementedCounters[index].value = incrementedCounters[index].value + 1;
    setCounters(incrementedCounters);
  }

  function decrement(countid,index) {
    if (counters[index].value > 0) {
      let decrementedCounters = [...counters];
      decrementedCounters[index].value =
        decrementedCounters[index].value - 1;
      setCounters(decrementedCounters);
    }
  }

  function deleteButton(countid) {
    let newCounters = counters.filter((counter) => counter.key !== countid);
    setCounters(newCounters);
  }

  return (
    <div className="container" countid={countid}>
      <span className={counters[index].value === 0 ? "zeroBox" : "countBox"}>
        {counters[index].value || "Zero"}
      </span>
      <button className="incrimentButton" onClick={() => increment(countid,index)}>
        <img src="/plus.png" className="plus" />
      </button>
      <button className="decrimentButton" onClick={() => decrement(countid,index)}>
        <img src="/minus.png" className="minus" />
      </button>
      <button className="deleteButton" onClick={() => deleteButton(countid)}>
        <img src="/delete.png" className="delete" />
      </button>
    </div>
  );
}
