import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import Navebar from "./navebar";
import RefreshBar from "./refreshBar";
import CounterList from "./counterList";

function App() {
  const [counters, setCounters] = useState([
    { value: 0, key: 0 },
    { value: 0, key: 1 },
    { value: 0, key: 2 },
    { value: 0, key: 3 },
  ]);

  return (
    <>
      <Navebar counters={counters} />
      <RefreshBar counters={counters} setCounters={setCounters}/>
      <CounterList counters={counters} setCounters={setCounters} />
    </>
  );
}

export default App;
